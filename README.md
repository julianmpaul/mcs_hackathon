# Cognitive Heuristic Intelligence for Development and Learning (C.H.I.L.D)
## Real-world Challenge
As a parent I can attest to the daunting task of keeping an ever watchful eye to my little one. Keeping harmful objects away from his reach, removing small objects that he can accidentally swallow, avoiding nasty falls, or even if he is already awake from his nap.

Its not all about danger though, catching the first time he tries to stand, walk, or dance. Seeing him learn from one of his toys or one of his books, learning that his favorite fruit is ripe mango or even just a simple smile. Being in constant view of the little guy does have its amazing moments.

Another benefit of the constant stream of information from watching over him is that it gives valuable insight into his development and care. How much he's grown, how often does he eat or drink, which visual cues does he respond to, even to how soundly he sleeps.

Now even if I do manage to be hawk-eyed, the reality is that eventually I would need to get some shut-eye or need to leave home and go to work. 

## Vision
It is my vision to build a system that would help every parent manage the herculean task of raising children. 
It augments parenting powers by providing constant environment processing to provide alerts to potential safety issues or notifications for any interesting developments. (potentially prevent SIDS or is he making a poop face?)
It is a tool that can provide visual data analysis to identify areas of interest and improvements.
It is a medium which can provide real time feedback to parents when they are away from home.
It captures those milestone moments so that they can be relived.

### Features
* Identify potentially harmful objects within child's vicinity
* Identify favorite objects (toys, images) based on visual and behavioral patterns
* Parent notifications
* Constant remote monitoring, alerting, and real time feedback
* Moment capture

### Good to have
* Nutrition monitoring
* Status and predictive mood detection
* Speech and Language processing and learning
* Possible integration with VR, AR, MR

### Constraints
* Start with one child for the initial phase
* Assumption that the app or device is in constant full view of the child and his/her surrounings

### Known issues
* Possible privacy concerns when applied to teens or adults

### Building upon the idea after the campaign
* Analyze, assess, and build each feature as a seperate module that integrates into the system. This eventually enables tapping into groups that specialize into that feature's tech stack.
* Distribute the MVP to other parents and capture metrics with consent for continous improvement.
* Integrate with rovers/IoT/wearables for protability/mobility and constant visibility.
* Possibly extend to the elderly.
* Possibly extend to pets.